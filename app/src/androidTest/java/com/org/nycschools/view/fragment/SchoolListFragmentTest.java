package com.org.nycschools.view.fragment;

import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;

import com.org.nycschools.R;
import com.org.nycschools.view.activity.MainActivity;

import junit.framework.AssertionFailedError;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.allOf;
import static org.junit.Assert.assertEquals;

/**
 * Created by mallakr on 2/24/2018.
 */

@MediumTest
@RunWith(AndroidJUnit4.class)
public class SchoolListFragmentTest {


    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void init() {
        activityActivityTestRule.getActivity()
                .getSupportFragmentManager().beginTransaction();
    }


    @Test
    public void testSchoolListVisibility() {
        onView(withId(R.id.schoolList)).check(matches((isDisplayed())));

    }

    @Test
    public void testClickSchoolItemLoadsSchoolDetailsFragment() {
        try {

            ViewInteraction viewInteraction = onView(withId(R.id.schoolList));
            viewInteraction.check(matches(isDisplayed()));
        } catch (NoMatchingViewException e) {
            return;
        } catch (AssertionFailedError e) {
            return;
        }
        //Here we are sure, that the schoolList recyclerview is visible to the user.
        onView(allOf(withId(R.id.schoolList), isDisplayed())).perform(RecyclerViewActions
                .actionOnItemAtPosition(1, click()));


        Fragment currentFragment = activityActivityTestRule.getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.fragmentContainer);
        assertEquals(currentFragment.getClass().getSimpleName(), SchoolDetailsFragment.class.getSimpleName());
    }
}



