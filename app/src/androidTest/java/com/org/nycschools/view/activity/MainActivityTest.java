package com.org.nycschools.view.activity;

import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.Toolbar;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.org.nycschools.R;
import com.org.nycschools.view.activity.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by mallakr on 2/24/2018.
 */

@MediumTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {


    @Rule
    public ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class);

    public MainActivityTest() {
        super(MainActivity.class);
    }



    @Test
    public void ensureToolbarIsPresent() throws Exception {
        MainActivity activity = rule.getActivity();
        View viewById = activity.findViewById(R.id.toolbarMain);
        assertThat(viewById, notNullValue());
        assertThat(viewById, instanceOf(Toolbar.class));
    }

    @Test
    public void ensureFragmentContainerIsPresent() throws Exception {
        MainActivity activity = rule.getActivity();
        View viewById = activity.findViewById(R.id.fragmentContainer);
        assertThat(viewById, notNullValue());
        assertThat(viewById, instanceOf(FrameLayout.class));
    }

}
