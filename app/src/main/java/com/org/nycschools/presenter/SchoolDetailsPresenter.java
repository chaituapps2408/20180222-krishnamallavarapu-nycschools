package com.org.nycschools.presenter;

import com.org.nycschools.model.SchoolDetails;
import com.org.nycschools.model.SchoolSatResults;
import com.org.nycschools.repository.Repository;
import com.org.nycschools.view.adapter.viewholder.ISchoolDetailsSatResultsViewHolder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mallakr on 2/23/2018.
 */

public class SchoolDetailsPresenter extends BasePresenter implements Callback<List<SchoolSatResults>> {

    private static final String TAG = SchoolDetailsPresenter.class.getSimpleName();

    Call<List<SchoolSatResults>> serviceCall;

    SchoolDetails selectedSchoolDetails;

    SchoolSatResults schoolSatResults;

    ISchoolDetailsSatResultsViewHolder iSchoolDetailsSatResultsViewHolder;

    public SchoolDetailsPresenter() {

        selectedSchoolDetails = Repository.getInstance().getSelectedSchool();

    }


    public void getSchoolSATResults(ISchoolDetailsSatResultsViewHolder iSchoolDetailsSatResultsViewHolder) {

        this.iSchoolDetailsSatResultsViewHolder = iSchoolDetailsSatResultsViewHolder;

        if (schoolSatResults != null) {
            displaySatResultsData(schoolSatResults);
            return;
        }

        iSchoolDetailsSatResultsViewHolder.hideResultsContainer();
        iSchoolDetailsSatResultsViewHolder.showLoadingSpinner();
        serviceCall = service.getSatResults(selectedSchoolDetails.getDbn());
        serviceCall.enqueue(this);

    }


    @Override
    public void onResponse(Call<List<SchoolSatResults>> call, Response<List<SchoolSatResults>> response) {

        List<SchoolSatResults> list = response.body();
        if (list != null && list.size() > 0) {
            schoolSatResults = list.get(0);
            displaySatResultsData(schoolSatResults);
        } else {
            displaySatErrorMessage();
        }


    }

    @Override
    public void onFailure(Call<List<SchoolSatResults>> call, Throwable t) {
        displaySatErrorMessage();
    }


    /**
     * method to display SAT data in view
     */
    public void displaySatResultsData(SchoolSatResults satResults) {
        iSchoolDetailsSatResultsViewHolder.hideLoadingSpinner();
        iSchoolDetailsSatResultsViewHolder.showResultsContainer();
        iSchoolDetailsSatResultsViewHolder.displayMathAvg(satResults.getSatMathAvgScore());
        iSchoolDetailsSatResultsViewHolder.displayReadingAvg(satResults.getSatCriticalReadingAvgScore());
        iSchoolDetailsSatResultsViewHolder.displayWritingAvg(satResults.getSatWritingAvgScore());
        iSchoolDetailsSatResultsViewHolder.displayTestTakers(satResults.getNumOfSatTestTakers());
    }

    /**
     * display error message if sat data is unavailable
     */
    public void displaySatErrorMessage() {
        iSchoolDetailsSatResultsViewHolder.hideLoadingSpinner();
        iSchoolDetailsSatResultsViewHolder.hideResultsContainer();
        iSchoolDetailsSatResultsViewHolder.displayErrorMessage();
    }

    public String getSchoolName() {

        return selectedSchoolDetails.getSchoolName();
    }

    public String getSchoolOverview() {

        return selectedSchoolDetails.getOverviewParagraph();
    }


    public String getSchoolAddress() {
        return new StringBuilder().append(selectedSchoolDetails.getPrimaryAddressLine1())
                .append(" , ")
                .append(selectedSchoolDetails.getCity())
                .append(" , ").append(selectedSchoolDetails.getStateCode())
                .toString();
    }

    public String getPhoneNumberValue() {
        return selectedSchoolDetails.getPhoneNumber();
    }

    public String getFaxNumberValue() {
        return selectedSchoolDetails.getFaxNumber();
    }

    public String getWebsiteValue() {
        return selectedSchoolDetails.getWebsite();
    }
}
