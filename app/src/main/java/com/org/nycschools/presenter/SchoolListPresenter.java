package com.org.nycschools.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.org.nycschools.model.SchoolDetails;
import com.org.nycschools.repository.Repository;
import com.org.nycschools.view.adapter.viewholder.ISchoolListViewHolder;
import com.org.nycschools.view.fragment.ISchoolListFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mallakr on 2/23/2018.
 */

public class SchoolListPresenter extends BasePresenter implements Callback<List<SchoolDetails>> {

    private static final String TAG = SchoolListPresenter.class.getSimpleName();
    public static final int LIMIT = 50;


    Call<List<SchoolDetails>> serviceCall;

    List<SchoolDetails> schoolList;

    ISchoolListFragment iSchoolListFragment;

    public SchoolListPresenter(@NonNull ISchoolListFragment listener) {
        iSchoolListFragment = listener;
    }


    public void getSchoolList() {

        if (schoolList != null) {
            displayData();
            return;
        }

        iSchoolListFragment.showLoadingSpinner();
        serviceCall = service.getNycSchoolList(LIMIT);
        serviceCall.enqueue(this);
    }


    @Override
    public void onResponse(Call<List<SchoolDetails>> call, Response<List<SchoolDetails>> response) {

        schoolList = response.body();
        //Log.d(TAG, String.valueOf(schoolList.size()));

        if (schoolList == null)
            displayError();
        else
            displayData();

    }

    private void displayData() {
        iSchoolListFragment.hideLoadingSpinner();
        iSchoolListFragment.displaySchoolList();
    }

    @Override
    public void onFailure(Call<List<SchoolDetails>> call, Throwable t) {

        displayError();
        Log.d(TAG, t.getMessage());

    }

    private void displayError() {
        iSchoolListFragment.hideLoadingSpinner();
        iSchoolListFragment.displayErrorMessage();
    }

    public void handleOnDestroy() {

        if (serviceCall != null && !serviceCall.isExecuted()) {
            serviceCall.cancel();
        }
    }


    public void onBindSchoolRowViewAtPosition(ISchoolListViewHolder rowView, int position) {
        SchoolDetails schoolDetails = schoolList.get(position);
        rowView.setSchoolName(schoolDetails.getSchoolName());
        rowView.setSchoolAddress(new StringBuilder().append(schoolDetails.getCity()).append(" ,").append(schoolDetails.getStateCode()).toString());
        rowView.setSchoolPhoneNumber(schoolDetails.getPhoneNumber());
        rowView.setSchoolWebAddress(schoolDetails.getWebsite());
    }

    public int getSchoolListCount() {
        return schoolList.size();
    }

    public void onItemClicked(int position) {

        Repository.getInstance().setSelectedSchool(schoolList.get(position));
        iSchoolListFragment.loadDetailsFragment();

    }
}
