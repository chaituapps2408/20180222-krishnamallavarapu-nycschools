package com.org.nycschools.presenter;

import android.support.annotation.CallSuper;

import com.org.nycschools.network.NetworkProvider;
import com.org.nycschools.network.NycSchoolsService;

/**
 * Created by mallakr on 2/23/2018.
 */

public class BasePresenter {

    NycSchoolsService service;

    BasePresenter() {
        service = NetworkProvider.getNetworkClient().create(NycSchoolsService.class);
    }


}
