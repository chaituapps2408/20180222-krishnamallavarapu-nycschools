package com.org.nycschools.repository;

import com.org.nycschools.model.SchoolDetails;

/**
 * Created by mallakr on 2/23/2018.
 * <p>
 * Repository class to hold data locally.
 * In this case its a singlton class but van be replaced with shared pref of sql db
 */

public class Repository {

    private static Repository repository;

    SchoolDetails selectedSchool;

    public static Repository getInstance() {

        if (repository == null)
            repository = new Repository();

        return repository;

    }


    public SchoolDetails getSelectedSchool() {
        return selectedSchool;
    }

    public void setSelectedSchool(SchoolDetails selectedSchool) {
        this.selectedSchool = selectedSchool;
    }
}
