package com.org.nycschools.view.fragment;

/**
 * Created by mallakr on 2/23/2018.
 * Interface for communication between presenter and SchoolListFragment
 */

public interface ISchoolListFragment {

    void showLoadingSpinner();

    void hideLoadingSpinner();

    void displaySchoolList();

    void displayErrorMessage();

    void loadDetailsFragment();
}
