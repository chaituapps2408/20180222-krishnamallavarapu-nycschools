package com.org.nycschools.view.adapter.viewholder;

import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.org.nycschools.R;
import com.org.nycschools.presenter.SchoolDetailsPresenter;

/**
 * Created by mallakr on 2/24/2018.
 */

public class SchoolDetailsSatResultsViewHolder extends SchoolDetailsViewHolder implements ISchoolDetailsSatResultsViewHolder {

    SchoolDetailsPresenter presenter;
    ConstraintLayout resultsContainer;
    ProgressBar progressBar;
    TextView errorMessage;
    TextView testTakersValue;
    TextView mathAvgValue;
    TextView readingAvgValue;
    TextView writingAvgValue;

    public SchoolDetailsSatResultsViewHolder(View itemView, SchoolDetailsPresenter presenter) {
        super(itemView, presenter);
        this.presenter = presenter;
        resultsContainer = itemView.findViewById(R.id.results_container);
        progressBar = itemView.findViewById(R.id.results_loading_spinner);
        errorMessage = itemView.findViewById(R.id.errorMessage);
        testTakersValue = itemView.findViewById(R.id.testTakenValue);
        mathAvgValue = itemView.findViewById(R.id.mathAvgValue);
        readingAvgValue = itemView.findViewById(R.id.readingAvgValue);
        writingAvgValue = itemView.findViewById(R.id.writingAvgValue);
    }

    public void setupData(int position) {
        presenter.getSchoolSATResults(this);
    }

    @Override
    public void showLoadingSpinner() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingSpinner() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void hideResultsContainer() {
        resultsContainer.setVisibility(View.GONE);
    }

    @Override
    public void showResultsContainer() {
        resultsContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayErrorMessage() {
        errorMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayTestTakers(String value) {
        testTakersValue.setText(value);
    }

    @Override
    public void displayMathAvg(String value) {
        mathAvgValue.setText(value);
    }

    @Override
    public void displayWritingAvg(String value) {
        writingAvgValue.setText(value);
    }

    @Override
    public void displayReadingAvg(String value) {
        readingAvgValue.setText(value);
    }
}
