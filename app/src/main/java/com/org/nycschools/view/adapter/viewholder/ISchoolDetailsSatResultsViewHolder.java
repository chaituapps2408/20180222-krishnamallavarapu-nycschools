package com.org.nycschools.view.adapter.viewholder;

/**
 * Created by mallakr on 2/24/2018.
 */

public interface ISchoolDetailsSatResultsViewHolder {

    void showLoadingSpinner();

    void hideLoadingSpinner();

    void hideResultsContainer();

    void showResultsContainer();

    void displayErrorMessage();

    void displayTestTakers(String value);

    void displayMathAvg(String value);

    void displayWritingAvg(String value);

    void displayReadingAvg(String value);
}
