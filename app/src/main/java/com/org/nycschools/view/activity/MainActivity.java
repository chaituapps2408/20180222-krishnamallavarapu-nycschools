package com.org.nycschools.view.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.org.nycschools.R;
import com.org.nycschools.view.fragment.SchoolListFragment;

/**
 * Created by mallakr on 2/23/2018.
 */

public class MainActivity extends BaseActivity {
    FrameLayout fragmentContainer;

    @Override
    public void intiLayout(Bundle savedInstanceState) {
        fragmentContainer = findViewById(R.id.fragmentContainer);
        toolbar = findViewById(R.id.toolbarMain);
        if (savedInstanceState == null) {
            loadFragment(SchoolListFragment.newInstance(), false);
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }
}
