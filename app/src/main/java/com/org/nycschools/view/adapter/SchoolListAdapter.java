package com.org.nycschools.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.org.nycschools.R;
import com.org.nycschools.presenter.SchoolListPresenter;
import com.org.nycschools.view.adapter.viewholder.SchoolListViewHolder;

/**
 * Created by mallakr on 2/23/2018.
 */

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListViewHolder> {


    private SchoolListPresenter presenter;

    public SchoolListAdapter(SchoolListPresenter presenter) {

        this.presenter = presenter;

    }

    @Override
    public SchoolListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.school_item, parent, false);
        return new SchoolListViewHolder(presenter, view);
    }

    @Override
    public void onBindViewHolder(SchoolListViewHolder holder, int position) {

        presenter.onBindSchoolRowViewAtPosition(holder, position);

    }

    @Override
    public int getItemCount() {
        return presenter.getSchoolListCount();
    }
}
