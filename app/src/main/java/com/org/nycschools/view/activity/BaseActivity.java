package com.org.nycschools.view.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.org.nycschools.R;
import com.org.nycschools.view.fragment.BaseFragment;

public abstract class BaseActivity extends AppCompatActivity {

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        intiLayout(savedInstanceState);
    }

    public abstract void intiLayout(Bundle savedInstanceState);

    public abstract int getLayout();


    public void loadFragment(BaseFragment fragment, boolean addToBackStack) {

        if (fragment == null)
            return;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (addToBackStack)
            ft.addToBackStack(fragment.getClass().getName());


        ft.replace(R.id.fragmentContainer, fragment, fragment.getClass().getName())
                .commit();


    }

    public void toolBarVisibility(int visibility) {
        toolbar.setVisibility(visibility);
    }

}
