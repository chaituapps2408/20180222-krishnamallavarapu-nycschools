package com.org.nycschools.view.fragment;

import android.support.annotation.NonNull;

import net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.org.nycschools.R;
import com.org.nycschools.presenter.SchoolDetailsPresenter;
import com.org.nycschools.view.activity.BaseActivity;
import com.org.nycschools.view.adapter.SchoolDetailsAdapter;

/**
 * Created by mallakr on 2/23/2018.
 */

public class SchoolDetailsFragment extends BaseFragment {


    RecyclerView detailsRecyclerView;
    SchoolDetailsPresenter presenter;

    public static SchoolDetailsFragment newInstance() {
        return new SchoolDetailsFragment();
    }


    @Override
    protected void initPresenter() {
        if (presenter == null) {
            presenter = new SchoolDetailsPresenter();
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.school_details_fragment;
    }

    @Override
    protected void initFragment(@NonNull View rootView) {


        detailsRecyclerView = rootView.findViewById(R.id.detailsRecyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        detailsRecyclerView.setLayoutManager(layoutManager);
        detailsRecyclerView.setAdapter(new SchoolDetailsAdapter(presenter));

        CollapsingToolbarLayout collapsingToolbar = rootView.findViewById(R.id.main_collapsing);
        collapsingToolbar.setTitle(presenter.getSchoolName());


        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null)
            activity.toolBarVisibility(View.GONE);

    }


}
