package com.org.nycschools.view.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.org.nycschools.R;
import com.org.nycschools.presenter.SchoolListPresenter;

import java.util.Random;

/**
 * Created by mallakr on 2/23/2018.
 */

public class SchoolListViewHolder extends RecyclerView.ViewHolder implements ISchoolListViewHolder, View.OnClickListener {

    TextView schoolName;
    TextView schoolAddress;
    TextView schoolPhoneNumber;
    TextView schoolWebsite;
    SchoolListPresenter presenter;

    public SchoolListViewHolder(SchoolListPresenter presenter, View itemView) {
        super(itemView);
        this.presenter = presenter;
        itemView.setOnClickListener(this);
        schoolName = itemView.findViewById(R.id.schoolName);
        schoolAddress = itemView.findViewById(R.id.schoolAddress);
        schoolPhoneNumber = itemView.findViewById(R.id.schoolPhoneNumber);
        schoolWebsite = itemView.findViewById(R.id.schoolWebsite);
    }

    @Override
    public void setSchoolName(String name) {
        schoolName.setText(name);
    }

    @Override
    public void setSchoolAddress(String address) {
        schoolAddress.setText(address);
    }

    @Override
    public void setSchoolPhoneNumber(String phoneNumber) {
        schoolPhoneNumber.setText(phoneNumber);
    }

    @Override
    public void setSchoolWebAddress(String webAddress) {
        schoolWebsite.setText(webAddress);
    }

    @Override
    public void onClick(View view) {
        presenter.onItemClicked(getAdapterPosition());
    }
}
