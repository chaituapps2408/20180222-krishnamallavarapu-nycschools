package com.org.nycschools.view.fragment;


import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.org.nycschools.R;
import com.org.nycschools.presenter.SchoolListPresenter;
import com.org.nycschools.view.activity.BaseActivity;
import com.org.nycschools.view.adapter.SchoolListAdapter;


/**
 * Created by mallakr on 2/23/2018.
 */

public class SchoolListFragment extends BaseFragment implements ISchoolListFragment {

    RecyclerView recyclerView;
    TextView errorMessage;
    SchoolListAdapter schoolListAdapter;
    ProgressBar loadingSpinner;
    SchoolListPresenter schoolListPresenter;

    public static SchoolListFragment newInstance() {
        return new SchoolListFragment();
    }


    /**
     * method to initialize presenter.
     */
    @Override
    protected void initPresenter() {
        if (schoolListPresenter == null)
            schoolListPresenter = new SchoolListPresenter(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.school_list_fragment;
    }

    @Override
    protected void initFragment(@NonNull View rootView) {

        errorMessage = rootView.findViewById(R.id.errorMessage);
        recyclerView = rootView.findViewById(R.id.schoolList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        loadingSpinner = rootView.findViewById(R.id.loading_spinner);
        schoolListPresenter.getSchoolList();

        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null)
            activity.toolBarVisibility(View.VISIBLE);
    }


    @Override
    public void showLoadingSpinner() {
        recyclerView.setVisibility(View.GONE);
        loadingSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingSpinner() {
        recyclerView.setVisibility(View.VISIBLE);
        loadingSpinner.setVisibility(View.GONE);
    }

    @Override
    public void displaySchoolList() {
        schoolListAdapter = new SchoolListAdapter(schoolListPresenter);
        recyclerView.setAdapter(schoolListAdapter);
    }

    @Override
    public void displayErrorMessage() {
        errorMessage.setVisibility(View.VISIBLE);
        Toast.makeText(getActivity(), R.string.school_list_error_message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void loadDetailsFragment() {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null)
            activity.loadFragment(SchoolDetailsFragment.newInstance(), true);
    }
}
