package com.org.nycschools.view.adapter.viewholder;

/**
 * Created by mallakr on 2/23/2018.
 */

public interface ISchoolListViewHolder {


    void setSchoolName(String name);

    void setSchoolAddress(String address);

    void setSchoolPhoneNumber(String phoneNumber);

    void setSchoolWebAddress(String webAddress);

}
