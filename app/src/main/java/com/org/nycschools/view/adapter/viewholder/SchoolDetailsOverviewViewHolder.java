package com.org.nycschools.view.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.org.nycschools.R;
import com.org.nycschools.presenter.SchoolDetailsPresenter;

/**
 * Created by mallakr on 2/24/2018.
 */

public class SchoolDetailsOverviewViewHolder extends SchoolDetailsViewHolder {

    SchoolDetailsPresenter presenter;
    TextView overviewTextView;

    public SchoolDetailsOverviewViewHolder(View itemView, SchoolDetailsPresenter presenter) {
        super(itemView, presenter);
        this.presenter = presenter;
        overviewTextView = itemView.findViewById(R.id.schoolOverview);
    }

    public void setData(int position) {

        overviewTextView.setText(presenter.getSchoolOverview());

    }

}
