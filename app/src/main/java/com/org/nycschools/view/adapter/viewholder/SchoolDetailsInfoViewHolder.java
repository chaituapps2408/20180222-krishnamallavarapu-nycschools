package com.org.nycschools.view.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.org.nycschools.R;
import com.org.nycschools.presenter.SchoolDetailsPresenter;

/**
 * Created by mallakr on 2/24/2018.
 */

public class SchoolDetailsInfoViewHolder extends SchoolDetailsViewHolder {

    SchoolDetailsPresenter presenter;
    TextView addressValue;
    TextView phoneNumberValue;
    TextView faxNumberValue;
    TextView websiteValue;

    public SchoolDetailsInfoViewHolder(View itemView, SchoolDetailsPresenter presenter) {
        super(itemView, presenter);
        this.presenter = presenter;
        addressValue = itemView.findViewById(R.id.addressValue);
        phoneNumberValue = itemView.findViewById(R.id.phoneNumberValue);
        faxNumberValue = itemView.findViewById(R.id.faxNumberValue);
        websiteValue = itemView.findViewById(R.id.websiteValue);
    }

    public void setupData(int position) {
        addressValue.setText(presenter.getSchoolAddress());
        phoneNumberValue.setText(presenter.getPhoneNumberValue());
        faxNumberValue.setText(presenter.getFaxNumberValue());
        websiteValue.setText(presenter.getWebsiteValue());

    }
}
