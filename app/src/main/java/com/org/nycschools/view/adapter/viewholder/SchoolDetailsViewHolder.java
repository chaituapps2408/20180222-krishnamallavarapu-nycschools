package com.org.nycschools.view.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.org.nycschools.presenter.BasePresenter;

/**
 * Created by mallakr on 2/24/2018.
 */

public class SchoolDetailsViewHolder extends RecyclerView.ViewHolder {

    BasePresenter presenter;

    public SchoolDetailsViewHolder(View itemView, BasePresenter presenter) {
        super(itemView);
        this.presenter = presenter;
    }

}
