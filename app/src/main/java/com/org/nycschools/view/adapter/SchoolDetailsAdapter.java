package com.org.nycschools.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.org.nycschools.R;
import com.org.nycschools.presenter.SchoolDetailsPresenter;
import com.org.nycschools.view.adapter.viewholder.SchoolDetailsInfoViewHolder;
import com.org.nycschools.view.adapter.viewholder.SchoolDetailsOverviewViewHolder;
import com.org.nycschools.view.adapter.viewholder.SchoolDetailsSatResultsViewHolder;
import com.org.nycschools.view.adapter.viewholder.SchoolDetailsViewHolder;

/**
 * Created by mallakr on 2/24/2018.
 */

public class SchoolDetailsAdapter extends RecyclerView.Adapter<SchoolDetailsViewHolder> {


    private static final int DETAILS_CARD_COUNT = 3;
    private static final int DETAILS_CARD_TYPE_OVERVIEW = 0;
    private static final int DETAILS_CARD_TYPE_SAT_RESULTS = 1;
    private static final int DETAILS_CARD_TYPE_CONTACT_INFO = 2;
    SchoolDetailsPresenter presenter;

    public SchoolDetailsAdapter(SchoolDetailsPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public SchoolDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case R.layout.school_details_overview_card:
                return new SchoolDetailsOverviewViewHolder(LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false), presenter);
            case R.layout.school_details_results_card:
                return new SchoolDetailsSatResultsViewHolder(LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false), presenter);
            case R.layout.school_details_info_card:
                return new SchoolDetailsInfoViewHolder(LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false), presenter);
            default:
                return new SchoolDetailsViewHolder(LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false), presenter);
        }

    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case DETAILS_CARD_TYPE_OVERVIEW:
                return R.layout.school_details_overview_card;
            case DETAILS_CARD_TYPE_SAT_RESULTS:
                return R.layout.school_details_results_card;
            case DETAILS_CARD_TYPE_CONTACT_INFO:
                return R.layout.school_details_info_card;
            default:
                return R.layout.school_details_overview_card;
        }
    }

    @Override
    public void onViewAttachedToWindow(SchoolDetailsViewHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(SchoolDetailsViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public void onBindViewHolder(SchoolDetailsViewHolder holder, int position) {

        if (holder instanceof SchoolDetailsOverviewViewHolder) {
            ((SchoolDetailsOverviewViewHolder) holder).setData(position);
        }

        if (holder instanceof SchoolDetailsInfoViewHolder) {
            ((SchoolDetailsInfoViewHolder) holder).setupData(position);
        }

        if (holder instanceof SchoolDetailsSatResultsViewHolder) {
            ((SchoolDetailsSatResultsViewHolder) holder).setupData(position);
        }

    }


    @Override
    public int getItemCount() {
        return DETAILS_CARD_COUNT;
    }


}
