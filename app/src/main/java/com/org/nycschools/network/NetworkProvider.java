package com.org.nycschools.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mallakr on 2/23/2018.
 * <p>
 * Class provides instance of network client
 */

public class NetworkProvider {

    private static final String BASE_URL = "https://data.cityofnewyork.us";
    public static final String API_KEY = "RmI13mtXNrpsmOekVmqvOOz28";

    private static Retrofit retrofit = null;

    /**
     * Creates network client
     *
     * @return Retrofit client
     */
    public static Retrofit getNetworkClient() {


        if (retrofit != null)
            return retrofit;

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        return retrofit;
    }

}
