package com.org.nycschools.network;

import com.org.nycschools.model.SchoolDetails;
import com.org.nycschools.model.SchoolSatResults;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by mallakr on 2/23/2018.
 * <p>
 * Service class contains backend apis
 */

public interface NycSchoolsService {


    /**
     * Method to get list of nyc schools.
     *
     * @param limit number of school to retrieve
     * @return <List<SchoolDetails>> list of schools
     */
    @Headers({
            "X-App-Token:" + NetworkProvider.API_KEY
    })
    @GET("resource/97mf-9njv.json")
    Call<List<SchoolDetails>> getNycSchoolList(@Query("$limit") int limit);

    /**
     * Method to get SAT results of particular school
     *
     * @param dbn unique id of school
     * @return SchoolSatResults SAT data of a school
     */
    @Headers({
            "X-App-Token:" + NetworkProvider.API_KEY
    })
    @GET("resource/734v-jeq5.json")
    Call<List<SchoolSatResults>> getSatResults(@Query("dbn") String dbn);

}
