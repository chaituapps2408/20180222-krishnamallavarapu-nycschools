package com.org.nycschools.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mallakr on 2/23/2018.
 *
 * model class to hold school data
 */

public class SchoolDetails {


    @SerializedName("academicopportunities1")
    @Expose
    public String academicopportunities1;
    @SerializedName("academicopportunities2")
    @Expose
    public String academicopportunities2;
    @SerializedName("admissionspriority11")
    @Expose
    public String admissionspriority11;
    @SerializedName("admissionspriority21")
    @Expose
    public String admissionspriority21;
    @SerializedName("admissionspriority31")
    @Expose
    public String admissionspriority31;
    @SerializedName("attendance_rate")
    @Expose
    public String attendanceRate;
    @SerializedName("bbl")
    @Expose
    public String bbl;
    @SerializedName("bin")
    @Expose
    public String bin;
    @SerializedName("boro")
    @Expose
    public String boro;
    @SerializedName("borough")
    @Expose
    public String borough;
    @SerializedName("building_code")
    @Expose
    public String buildingCode;
    @SerializedName("bus")
    @Expose
    public String bus;
    @SerializedName("census_tract")
    @Expose
    public String censusTract;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("code1")
    @Expose
    public String code1;
    @SerializedName("community_board")
    @Expose
    public String communityBoard;
    @SerializedName("council_district")
    @Expose
    public String councilDistrict;
    @SerializedName("dbn")
    @Expose
    public String dbn;
    @SerializedName("directions1")
    @Expose
    public String directions1;
    @SerializedName("ell_programs")
    @Expose
    public String ellPrograms;
    @SerializedName("extracurricular_activities")
    @Expose
    public String extracurricularActivities;
    @SerializedName("fax_number")
    @Expose
    public String faxNumber;
    @SerializedName("finalgrades")
    @Expose
    public String finalgrades;
    @SerializedName("grade9geapplicants1")
    @Expose
    public String grade9geapplicants1;
    @SerializedName("grade9geapplicantsperseat1")
    @Expose
    public String grade9geapplicantsperseat1;
    @SerializedName("grade9gefilledflag1")
    @Expose
    public String grade9gefilledflag1;
    @SerializedName("grade9swdapplicants1")
    @Expose
    public String grade9swdapplicants1;
    @SerializedName("grade9swdapplicantsperseat1")
    @Expose
    public String grade9swdapplicantsperseat1;
    @SerializedName("grade9swdfilledflag1")
    @Expose
    public String grade9swdfilledflag1;
    @SerializedName("grades2018")
    @Expose
    public String grades2018;
    @SerializedName("interest1")
    @Expose
    public String interest1;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("location")
    @Expose
    public String location;
    @SerializedName("longitude")
    @Expose
    public String longitude;
    @SerializedName("method1")
    @Expose
    public String method1;
    @SerializedName("neighborhood")
    @Expose
    public String neighborhood;
    @SerializedName("nta")
    @Expose
    public String nta;
    @SerializedName("offer_rate1")
    @Expose
    public String offerRate1;
    @SerializedName("overview_paragraph")
    @Expose
    public String overviewParagraph;
    @SerializedName("pct_stu_enough_variety")
    @Expose
    public String pctStuEnoughVariety;
    @SerializedName("pct_stu_safe")
    @Expose
    public String pctStuSafe;
    @SerializedName("phone_number")
    @Expose
    public String phoneNumber;
    @SerializedName("primary_address_line_1")
    @Expose
    public String primaryAddressLine1;
    @SerializedName("program1")
    @Expose
    public String program1;
    @SerializedName("requirement1_1")
    @Expose
    public String requirement11;
    @SerializedName("requirement2_1")
    @Expose
    public String requirement21;
    @SerializedName("requirement3_1")
    @Expose
    public String requirement31;
    @SerializedName("requirement4_1")
    @Expose
    public String requirement41;
    @SerializedName("requirement5_1")
    @Expose
    public String requirement51;
    @SerializedName("school_10th_seats")
    @Expose
    public String school10thSeats;
    @SerializedName("school_accessibility_description")
    @Expose
    public String schoolAccessibilityDescription;
    @SerializedName("school_email")
    @Expose
    public String schoolEmail;
    @SerializedName("school_name")
    @Expose
    public String schoolName;
    @SerializedName("school_sports")
    @Expose
    public String schoolSports;
    @SerializedName("seats101")
    @Expose
    public String seats101;
    @SerializedName("seats9ge1")
    @Expose
    public String seats9ge1;
    @SerializedName("seats9swd1")
    @Expose
    public String seats9swd1;
    @SerializedName("state_code")
    @Expose
    public String stateCode;
    @SerializedName("subway")
    @Expose
    public String subway;
    @SerializedName("total_students")
    @Expose
    public String totalStudents;
    @SerializedName("website")
    @Expose
    public String website;
    @SerializedName("zip")
    @Expose
    public String zip;

    public String getAcademicopportunities1() {
        return academicopportunities1;
    }

    public String getAcademicopportunities2() {
        return academicopportunities2;
    }

    public String getAdmissionspriority11() {
        return admissionspriority11;
    }

    public String getAdmissionspriority21() {
        return admissionspriority21;
    }

    public String getAdmissionspriority31() {
        return admissionspriority31;
    }

    public String getAttendanceRate() {
        return attendanceRate;
    }

    public String getBbl() {
        return bbl;
    }

    public String getBin() {
        return bin;
    }

    public String getBoro() {
        return boro;
    }

    public String getBorough() {
        return borough;
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public String getBus() {
        return bus;
    }

    public String getCensusTract() {
        return censusTract;
    }

    public String getCity() {
        return city;
    }

    public String getCode1() {
        return code1;
    }

    public String getCommunityBoard() {
        return communityBoard;
    }

    public String getCouncilDistrict() {
        return councilDistrict;
    }

    public String getDbn() {
        return dbn;
    }

    public String getDirections1() {
        return directions1;
    }

    public String getEllPrograms() {
        return ellPrograms;
    }

    public String getExtracurricularActivities() {
        return extracurricularActivities;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public String getFinalgrades() {
        return finalgrades;
    }

    public String getGrade9geapplicants1() {
        return grade9geapplicants1;
    }

    public String getGrade9geapplicantsperseat1() {
        return grade9geapplicantsperseat1;
    }

    public String getGrade9gefilledflag1() {
        return grade9gefilledflag1;
    }

    public String getGrade9swdapplicants1() {
        return grade9swdapplicants1;
    }

    public String getGrade9swdapplicantsperseat1() {
        return grade9swdapplicantsperseat1;
    }

    public String getGrade9swdfilledflag1() {
        return grade9swdfilledflag1;
    }

    public String getGrades2018() {
        return grades2018;
    }

    public String getInterest1() {
        return interest1;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLocation() {
        return location;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getMethod1() {
        return method1;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public String getNta() {
        return nta;
    }

    public String getOfferRate1() {
        return offerRate1;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public String getPctStuEnoughVariety() {
        return pctStuEnoughVariety;
    }

    public String getPctStuSafe() {
        return pctStuSafe;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPrimaryAddressLine1() {
        return primaryAddressLine1;
    }

    public String getProgram1() {
        return program1;
    }

    public String getRequirement11() {
        return requirement11;
    }

    public String getRequirement21() {
        return requirement21;
    }

    public String getRequirement31() {
        return requirement31;
    }

    public String getRequirement41() {
        return requirement41;
    }

    public String getRequirement51() {
        return requirement51;
    }

    public String getSchool10thSeats() {
        return school10thSeats;
    }

    public String getSchoolAccessibilityDescription() {
        return schoolAccessibilityDescription;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getSchoolSports() {
        return schoolSports;
    }

    public String getSeats101() {
        return seats101;
    }

    public String getSeats9ge1() {
        return seats9ge1;
    }

    public String getSeats9swd1() {
        return seats9swd1;
    }

    public String getStateCode() {
        return stateCode;
    }

    public String getSubway() {
        return subway;
    }

    public String getTotalStudents() {
        return totalStudents;
    }

    public String getWebsite() {
        return website;
    }

    public String getZip() {
        return zip;
    }
}

